import { defineConfig, loadEnv } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'
import viteCompression from 'vite-plugin-compression'

function _resolve(dir: string) {
  return path.resolve(__dirname, dir)
}
export default defineConfig({
  plugins: [
    vue(),
    viteCompression()
  ],
  resolve: {
    alias: {
      '@': _resolve('src'),
    },
    extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
  },
  build:{
    minify: 'terser',
    terserOptions: {
      compress: {
          //生产环境时移除console.log()
          drop_console: true,
          drop_debugger: true,
      },
    },
  },
})
