import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', {
  state: () => {
    return { count: 111111 }
  },
  // could also be defined as
  // state: () => ({ count: 0 })
  actions: {
    increment() {
      this.count++
    },
    getData() {
      return new Promise((resolve: any, reject: any) => {
        resolve([
          {
            id: 1,
            name: 'tom',
            age: 18
          },
          {
            id: 2,
            name: 'jerry',
            age: 14
          },
          {
            id: 3,
            name: 'spark',
            age: 22
          }
        ])
      })
    }
  },
})