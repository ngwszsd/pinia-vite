import { createRouter,  createWebHistory } from 'vue-router'
import Demo from '@/pages/Demo/index.vue'
import PdfView from '@/pages/PdfView/index.vue'
const routes = [
    {
        path: '/',
        redirect: 'demo'
    }, 
    {
        path: '/demo',
        component: Demo
    },
    {
        path: '/PdfView',
        component: PdfView
    }
]

// 创建路由
const router = createRouter({
    history: createWebHistory(),
    routes
})

// 导出路由
export default router